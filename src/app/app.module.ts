import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from "@angular/router";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { MatIconModule } from "@angular/material/icon";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatCardModule } from "@angular/material/card";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { KeycloakInitializer } from "./@auth/keycloak";
import { HomeComponent } from './pages/home/home.component';
import { MatDividerModule } from "@angular/material/divider";
import { MatListModule } from "@angular/material/list";
import { UserCreateComponent } from './pages/user-create/user-create.component';
import { HttpClientModule } from "@angular/common/http";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { UserListComponent } from './pages/user-list/user-list.component';
import { MatDialogModule } from "@angular/material/dialog";
import { environment } from 'src/environments/environment';
import { MatTableModule } from "@angular/material/table";
import { ApiGatewayModule } from "./services/api-gateway.module";
import { API_BASE_URL } from "./services/api-gateway";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTooltipModule } from "@angular/material/tooltip";
import { UserDeleteDialogComponent } from './dialogs/user-delete-dialog/user-delete-dialog.component';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { UserEditDialogComponent } from './dialogs/user-edit-dialog/user-edit-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserCreateComponent,
    UserListComponent,
    UserDeleteDialogComponent,
    UserEditDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    MatToolbarModule,
    AppRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    KeycloakAngularModule,
    MatDividerModule,
    MatListModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    MatCheckboxModule,
    MatTableModule,
    ApiGatewayModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatSnackBarModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: KeycloakInitializer,
      multi: true,
      deps: [KeycloakService],
    },
    {
      provide: API_BASE_URL,
      useValue: environment.apiBaseUrl
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
