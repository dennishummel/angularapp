import { Component, Inject } from '@angular/core';
import { TenantUserResponse, UpdateTenantUserCommand } from "../../services/api-gateway";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-user-edit-dialog',
  templateUrl: './user-edit-dialog.component.html',
  styleUrls: ['./user-edit-dialog.component.css']
})
export class UserEditDialogComponent {
  command: UpdateTenantUserCommand = new UpdateTenantUserCommand();

  constructor(@Inject(MAT_DIALOG_DATA) public data: TenantUserResponse, public dialogRef: MatDialogRef<UserEditDialogComponent>) {
    this.command.firstName = this.data.firstName;
    this.command.lastName = this.data.lastName;
    this.command.phone = this.data.phone;
    this.command.isActive = this.data.isActive;
  }

  updateUser() {
    this.dialogRef.close(this.command);
  }
}
