import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { KeycloakService } from "keycloak-angular";
import { MENU_ITEMS } from "./pages/pages-menu";
import { MenuItem } from "./models/menu";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  userProfile: any = {};
  menuItems: MenuItem[] = MENU_ITEMS;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private keycloakService: KeycloakService
  ) { }

  ngOnInit() {
    this.keycloakService.loadUserProfile().then(user => {
      this.userProfile = user;
      console.log(user);
    });
  }

  async logout() {
    await this.keycloakService.logout();
  }
}
