import { NgModule } from "@angular/core";
import { ApiGatewayClient } from "./api-gateway";

@NgModule({
  providers: [
    ApiGatewayClient
  ]
})
export class ApiGatewayModule { }
