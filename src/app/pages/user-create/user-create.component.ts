import { Component, Input, OnInit } from '@angular/core';
import {ApiGatewayClient, CreateTenantUserCommand} from "../../services/api-gateway";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  @Input() command: CreateTenantUserCommand = new CreateTenantUserCommand();

  constructor(
    private apiGatewayClient: ApiGatewayClient,
    public dialogRef: MatDialogRef<UserCreateComponent>
  ) {
  }

  ngOnInit(): void {
    this.command.organizationId = 1;
  }

  addUser() {
    this.apiGatewayClient.createTenantUser(this.command).subscribe(res => {
      this.dialogRef.close(true);
    });
  }
}
