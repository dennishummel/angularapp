import {MenuItem} from "../models/menu";

export const MENU_ITEMS: MenuItem[] = [
  {
    title: 'Home',
    link: '/'
  },
  {
    title: 'User Management',
    link: '/users'
  }
]
