import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { UserCreateComponent } from "../user-create/user-create.component";
import {
  ApiGatewayClient,
  TenantUserPaginatedResponse,
  TenantUserResponse,
  UpdateTenantUserCommand
} from "../../services/api-gateway";
import { UserDeleteDialogComponent } from "../../dialogs/user-delete-dialog/user-delete-dialog.component";
import { MatSnackBar } from "@angular/material/snack-bar";
import {UserEditDialogComponent} from "../../dialogs/user-edit-dialog/user-edit-dialog.component";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    public apiGatewayClient: ApiGatewayClient,
    private _snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.loadUserList()
  }

  columNames: string[] = ['id', 'firstName', 'lastName', 'email', 'actions']
  userListResponse: TenantUserPaginatedResponse | undefined
  userList: TenantUserResponse[] = []

  loadUserList() {
    this.apiGatewayClient.queryTenantUsers(1,
      undefined,
      undefined,
      undefined,
      1, 30,
      undefined,
      undefined)
      .subscribe(x =>
      {
        this.userListResponse = x;
        this.userList = x.result || [];
      })
  }

  openCreateUserDialog() {
    let dialog = this.dialog.open(UserCreateComponent, {
      width: '400px'
    })
    dialog.afterClosed().subscribe(res => {
      if (res !== true) {
        return;
      }
      this._snackBar.open('The user has been created successfully.', '',{
        duration: 3000
      })
      this.loadUserList();
    });
  }

  editUser(user: TenantUserResponse) {
    if (user.id == undefined) {
      return
    }
    let dialog = this.dialog.open(UserEditDialogComponent, {
      width: '400px',
      data: user
    })
    dialog.afterClosed().subscribe(result => {
      if (user.id === undefined) {
        return;
      }
      if (result instanceof UpdateTenantUserCommand) {
        this.apiGatewayClient.updateTenantUser(user.id, 1, result).subscribe(() => {
          this._snackBar.open('The user has been updated successfully.', '',{
            duration: 3000
          })
          this.loadUserList()
        });
      }
    })
  }

  deleteUser(user: TenantUserResponse) {
    let dialog = this.dialog.open(UserDeleteDialogComponent)
    dialog.afterClosed().subscribe(result => {
      if (result !== true) {
        return
      }
      if (user.id === undefined) {
        return
      }
      this.apiGatewayClient.deleteTenantUser(user.id, 1).subscribe(() =>{
        this._snackBar.open('The user has been deleted successfully.', '',{
          duration: 3000
        })
        this.loadUserList()
      })
    })
  }
}
