export class MenuItem {
  title: string = "";
  link: string = "";
  home?: boolean;
}
