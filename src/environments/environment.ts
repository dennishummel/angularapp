export const environment = {
  production: false,

  apiBaseUrl: 'http://localhost:5001',

  keycloak: {
    url: 'http://localhost:8080/auth',
    realm: 'MSLC20',
    clientId: 'frontend-client'
  }
};
